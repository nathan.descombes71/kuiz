import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:crypto/crypto.dart';
import 'package:kuiz/presentation/core/helpers/constant.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'dart:math' as math;

import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:url_launcher/url_launcher.dart';


// void showStatusBar() {
//   SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
//     statusBarColor: kPrimaryMiddleBlueColor,
//     statusBarIconBrightness: Brightness.light,
//     systemNavigationBarColor: kPrimaryMiddleBlueColor,
//   ));
//   SystemChrome.setEnabledSystemUIOverlays(
//       [SystemUiOverlay.bottom, SystemUiOverlay.top]);
// }

void hideStatusBar() {
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
}

launchURL(url) async {
  if (!await launch(url)) throw 'Could not launch $url';
}

toast(BuildContext context, String message,
    {bool success = false, bool error = false}) {
  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));

  if (success) {
    showTopSnackBar(
      context,
      CustomSnackBar.success(
        message: message,
        messagePadding: const EdgeInsets.symmetric(horizontal: 12),
        icon:
            const Icon(Icons.info_outline, color: Color(0x15000000), size: 120),
      ),
    );
  } else if (error) {
    showTopSnackBar(
      context,
      CustomSnackBar.error(
          message: message,
          messagePadding: const EdgeInsets.symmetric(horizontal: 12),
          icon: const Icon(Icons.info_outline,
              color: Color(0x15000000), size: 120)),
    );
  } else {
    showTopSnackBar(
      context,
      CustomSnackBar.info(
          message: message,
          messagePadding: const EdgeInsets.symmetric(horizontal: 12),
          icon: const Icon(Icons.info_outline,
              color: Color(0x15000000), size: 120),
          backgroundColor: Get.theme.colorScheme.secondary.withOpacity(0.9)),
    );
  }
}

String getAPIUrl() {
  if (isRelease) {
    return "https://us-central1-kuiz-934b6.cloudfunctions.net/v1";
  } else {
    return "http://10.0.2.2:5001/kuiz-934b6/us-central1/v1";
  }
}

Widget dialogTuto(int step) {
    switch (step) {
      case 0:
        return Dialog(
          child: IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20, right: 10),
              color: Get.theme.colorScheme.background,
              // height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  roundButton(Icons.dangerous, 150, false, 100, onPressed: () {
                    debugPrint("J'aime pas du tout !");
                  }),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Absolument pas d'accord !",
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Get
                              .theme.colorScheme.onSecondary
                              .withOpacity(0.7)),
                        ),
                        onPressed: () {
                          Get.back();
                          Future.delayed(Duration(milliseconds: 300), () {
                            Get.dialog(dialogTuto(1));
                          });
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              "Suite",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        return Dialog(
          child: IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20, right: 10),
              color: Get.theme.colorScheme.background,
              // height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  roundButton(Icons.thumb_down, 120, false, 80,
                      onPressed: () {}),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Pas d'accord !",
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Get
                              .theme.colorScheme.onSecondary
                              .withOpacity(0.7)),
                        ),
                        onPressed: () {
                          Get.back();
                          Future.delayed(Duration(milliseconds: 300), () {
                            Get.dialog(dialogTuto(2),  barrierDismissible: false);
                          });
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              "Suite",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        return Dialog(
          child: IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20, right: 10, left: 10),
              color: Get.theme.colorScheme.background,
              // height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  roundButton(Icons.format_list_bulleted, 120, true, 80,
                      onPressed: () {}),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Voir le classement des réponses",
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Get
                              .theme.colorScheme.onSecondary
                              .withOpacity(0.7)),
                        ),
                        onPressed: () {
                          Get.back();
                          Future.delayed(Duration(milliseconds: 300), () {
                            Get.dialog(dialogTuto(3),  barrierDismissible: false);
                          });
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              "Suite",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        return Dialog(
          child: IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20, right: 10),
              color: Get.theme.colorScheme.background,
              // height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  roundButton(Icons.thumb_up, 120, false, 80, onPressed: () {}),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "D'accord !",
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Get
                              .theme.colorScheme.onSecondary
                              .withOpacity(0.7)),
                        ),
                        onPressed: () {
                          Get.back();
                          Future.delayed(Duration(milliseconds: 300), () {
                            Get.dialog(dialogTuto(4), barrierDismissible: false);
                          });
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              "Suite",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        return Dialog(
          child: IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20, right: 10),
              color: Get.theme.colorScheme.background,
              // height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  roundButton(Icons.star, 150, false, 100, onPressed: () {}),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Parfaitement d'accord !",
                    style: TextStyle(fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Get
                              .theme.colorScheme.onSecondary
                              .withOpacity(0.7)),
                        ),
                        onPressed: () {
                          Get.back();
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text(
                              "Suite",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(Icons.chevron_right)
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Dialog(
          child: Container(
            child: Text("step" + step.toString()),
          ),
        );
    }
  }

  Widget roundButton(
      IconData iconToShow, double size, bool isMiddleButton, double iconSize,
      {required Function() onPressed}) {
    return Card(
      elevation: 7,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100),
      ),
      child: Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: isMiddleButton
              ? Get.theme.colorScheme.secondary.withOpacity(0.9)
              : Get.theme.colorScheme.onSecondary.withOpacity(0.7),
        ),
        child: Center(
          child: Icon(
            iconToShow,
            size: iconSize,
            color: Colors.white,
          ),
        ),
      ),
    );
  }





















