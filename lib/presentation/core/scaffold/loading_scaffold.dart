import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoadingScaffold extends StatefulWidget {
  const LoadingScaffold({
    Key? key,
  }) : super(key: key);

  @override
  State<LoadingScaffold> createState() => _LoadingScaffoldState();
}

class _LoadingScaffoldState extends State<LoadingScaffold> {
  RxDouble percentProgress = 0.0.obs;
  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 100), () {
      percentProgress.value = 200.0;
    });

    Timer.periodic(Duration(seconds: 3), (timer) {
      percentProgress.value = 0.0;
      Future.delayed(Duration(milliseconds: 100), () {
        percentProgress.value = 200.0;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Get.theme.colorScheme.primary,
      body: SizedBox(
        child: Center(
            child: Container(
          height: 50,
          width: 200,
          decoration: BoxDecoration(
              border: Border.all(),
              borderRadius: const BorderRadius.all(Radius.circular(15))),
          child: Obx(() {
            return Align(
              alignment: Alignment.centerLeft,
              child: AnimatedContainer(
                margin: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: Get.theme.colorScheme.secondary,
                    border: Border.all(),
                    borderRadius: const BorderRadius.all(Radius.circular(13))),
                width: percentProgress.value,
                // height: 45,
                duration: Duration(seconds: 2),
              ),
            );
          }),
        )),
      ),
    );
  }
}
