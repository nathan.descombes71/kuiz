
import 'package:kuiz/presentation/views/edit/edit_view.dart';
import 'package:kuiz/presentation/views/edit/edit_view_controller_bindings.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:kuiz/presentation/views/list_quizz/list_quizz_view.dart';
import '../views/list_quizz/list_quizz_view_controller_bindings.dart';
import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    /// REVIEW [Mobile & Web] routes
    GetPage(
      name: Routes.EDIT,
      page: () => EditView(),
      binding: EditViewControllerBindings(),
    ),
    GetPage(
      name: Routes.LISTQUIZZ,
      page: () => ListQuizzView(),
      binding: ListQuizzViewControllerBindings(),
    ),
  ];
}
