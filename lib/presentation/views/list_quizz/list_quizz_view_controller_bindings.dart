import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/controller/quizz_controller.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/domain/feature/quizz/repositories/quizz_repositories.dart';
import 'package:kuiz/infrastructure/api/rest_api_client.dart';
import 'package:kuiz/infrastructure/repositories/quizz_repositories_impl.dart';

import 'list_quizz_view_controller.dart';

class ListQuizzViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ListQuizzViewController(
        Get.put<QuizzController>(
          QuizzController(
            quizzRepository: Get.put<QuizzRepository>(
              QuizzRepositoryImpl(
                client: Get.find<RestApiClient>().client,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
