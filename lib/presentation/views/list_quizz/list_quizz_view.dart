import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/presentation/core/scaffold/loading_scaffold.dart';

import '../../../domain/feature/quizz/entities/quizz.dart';
import '../../navigation/routes.dart';
import 'list_quizz_view_controller.dart';

class ListQuizzView extends GetView<ListQuizzViewController> {
  ListQuizzView({Key? key}) : super(key: key);

  final GlobalKey stackKey = GlobalKey();

  double? pX;
  @override
  Widget build(BuildContext context) {
    return controller.obx(
        (state) => Scaffold(
              backgroundColor: Get.theme.colorScheme.primary,
              body: Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0, bottom: 30.0),
                      child: Image.asset(
                        'assets/graphic/logo.png',
                        width: Get.width * 0.7,
                      ),
                    ),
                    Divider(
                      color: Colors.white,
                      endIndent: Get.width * 0.2,
                      indent: Get.width * 0.2,
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Wrap(
                      runSpacing: 10,
                      spacing: 10,
                      children: List.generate(controller.allQuizz.length,
                          (index) => cardQuizz(controller.allQuizz[index])),
                    ),
                  ],
                ),
              ),
            ),
        onLoading: const LoadingScaffold());
  }

  Widget cardQuizz(Quizz actualQuizz) {
    return InkWell(
      onTap: () {
        Get.toNamed(Routes.EDIT, arguments: {"quizz": actualQuizz});
      },
      child: Column(
        children: [
          Card(
            elevation: 7,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(80),
            ),
            child: Container(
              height: 100,
              width: 100,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(17),
                child: Image.network(
                  actualQuizz.pathPhoto ?? "",
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                  loadingBuilder: (ctx, wdgt, chnkevt) {
                    if (chnkevt?.cumulativeBytesLoaded ==
                        chnkevt?.expectedTotalBytes) {
                      return wdgt;
                    } else {
                      return const SizedBox(
                          height: 100,
                          width: 100,
                          child: Center(
                            child: CircularProgressIndicator(
                              color: Colors.white,
                            ),
                          ));
                    }
                  },
                ),
              ),
            ),
          ),
          Text(
            actualQuizz.labelQuizz ?? "",
            style: TextStyle(color: Colors.white),
          )
        ],
      ),
    );
  }
}
