import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:kuiz/domain/feature/quizz/controller/quizz_controller.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';

class ListQuizzViewController extends GetxController with StateMixin {
  final QuizzController quizzController;

  ListQuizzViewController(this.quizzController);

  List<Quizz> allQuizz = [];

  Quizz? quizz;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());
    final box = GetStorage();

    String? idUser = box.read("idUser");

    

    allQuizz = await quizzController.getAllQuizz();
    // quizz = await quizzController.getOneQuizz(
    //     quizzID: "kyDWmSK1WveplXXn6Xgp",
    //     queryParameters: {"userId": "d45b7e70-1720-4b16-b7db-f7130bdc5bde"});

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }
}
