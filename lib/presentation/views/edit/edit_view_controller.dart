import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:kuiz/domain/feature/answer/controller/answer_controller.dart';
import 'package:kuiz/domain/feature/answer_user.dart/controller/answer_user_controller.dart';
import 'package:kuiz/domain/feature/answer_user.dart/entities/answer_user.dart';
import 'package:kuiz/domain/feature/questions/controller/questions_controller.dart';
import 'package:kuiz/domain/feature/quizz/controller/quizz_controller.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';

import '../../../domain/feature/answer/entities/answer.dart';
import '../../../domain/feature/questions/entities/questions.dart';
import '../../core/helpers/utils.dart';

class EditViewController extends GetxController with StateMixin {
  EditViewController(
      this.quizzController, this.questionController, this.answerController);

  PageController pageController = PageController();

  final GlobalKey stackKey = GlobalKey();

  final QuestionController questionController;

  final AnswerController answerController;

  final QuizzController quizzController;

  final box = GetStorage();

  Quizz? viewQuizz;

  RxInt animationTime = 0.obs;

  final RxInt currentIndex = 0.obs;

  RxDouble positionDragX = 190.0.obs;

  RxDouble xPosition = (Get.width * 0.11).obs;

  List<Questions> listOfQuestions = [];

  bool canSlide = true;

  List<Answer> listAnswer = [];

  RxList<AnswerUser> listAnswerUser = <AnswerUser>[].obs;

  Questions selectedQuestions = Questions();

  String? idUser = "";

  RxBool containQuestions = false.obs;

  bool? firstTime = true;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    idUser = box.read("idUser");
    firstTime = box.read("firstTime") ?? true;

    viewQuizz = Get.arguments["quizz"];

    await questionController.initQuestion(
        quizzID: viewQuizz?.id, userID: idUser);
    await answerController.initAnswer(quizzID: viewQuizz?.id, userID: idUser);

    viewQuizz = await quizzController.getOneQuizz(
        quizzID: viewQuizz?.id, queryParameters: {"userId": idUser});

    initList();

    listOfQuestions.shuffle();

    listOfQuestions.forEach((element) {
      if (element.isAnswered == false) {
        element.xPosition = (Get.width * 0.11).obs;
        element.durationTime = 0.obs;
        containQuestions.value = true;
      } else {
        element.xPosition = (Get.width * 2).obs;
        element.durationTime = 0.obs;
      }
    });
    selectedQuestions = listOfQuestions.last;

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    refresh();

    if (firstTime == true) {
      await Get.dialog(dialogTuto(0), barrierDismissible: false);
       box.write("firstTime", false);
    }

   

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void initList() {
    listOfQuestions = viewQuizz?.questionsList ?? [];

    listAnswer = viewQuizz?.answersList ?? [];

    for (Answer answer in listAnswer) {
      AnswerUser answerToAdd = AnswerUser(
          idAnswer: answer.id, answer: answer, points: answer.points);

      listAnswerUser.add(answerToAdd);
    }
  }

  goLeft(int point, {bool swipe = false}) async {
     canSlide = false;
    if (swipe == false) {
      selectedQuestions.durationTime?.value = 800;
      selectedQuestions.xPosition?.value = Get.width * -2;

      Future.delayed(const Duration(milliseconds: 900), () {
        selectedQuestions.durationTime?.value = 0;
      });
    }

    listAnswerUser.firstWhere((element) {
      return element.idAnswer == selectedQuestions.idAnswer;
    }).points = (listAnswerUser
                .firstWhere(
                    (element) => element.idAnswer == selectedQuestions.idAnswer)
                .points ??
            0) +
        point;

    await answerController.addPoints(
        points: point, answerID: selectedQuestions.idAnswer, userID: idUser);
    await questionController.answerQuestion(
        userID: idUser, questionID: selectedQuestions.id);

    nextQuestion();

    Questions? questionExist = listOfQuestions.firstWhere(
      (element) {
        return element.isAnswered == false;
      },
      orElse: () {
        return Questions();
      },
    );

    containQuestions.value = questionExist.id != null;

    print(containQuestions);
       Future.delayed(Duration(milliseconds: 500), (){
      canSlide = true;
    });
  }

  goRight(int point, {bool swipe = false}) async {
    canSlide = false;
    if (swipe == false) {
      selectedQuestions.durationTime?.value = 800;
      selectedQuestions.xPosition?.value = Get.width * 2;

      Future.delayed(const Duration(milliseconds: 900), () {
        selectedQuestions.durationTime?.value = 0;
      });
    }

    listAnswerUser.firstWhere((element) {
      return element.idAnswer == selectedQuestions.idAnswer;
    }).points = (listAnswerUser
                .firstWhere(
                    (element) => element.idAnswer == selectedQuestions.idAnswer)
                .points ??
            0) +
        point;

    await answerController.addPoints(
        points: point, answerID: selectedQuestions.idAnswer, userID: idUser);
    await questionController.answerQuestion(
        userID: idUser, questionID: selectedQuestions.id);

    nextQuestion();

    Questions? questionExist = listOfQuestions.firstWhere(
      (element) {
        return element.isAnswered == false;
      },
      orElse: () {
        return Questions();
      },
    );

    containQuestions.value = questionExist.id != null;

    Future.delayed(Duration(milliseconds: 500), (){
      canSlide = true;
    });
  }

  nextQuestion() {
    listOfQuestions
        .firstWhere((element) => element.id == selectedQuestions.id)
        .isAnswered = true;
    selectedQuestions = listOfQuestions.lastWhere(
        (element) => element.isAnswered == false,
        orElse: () => Questions());
  }
}
