import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../domain/feature/answer/entities/answer.dart';
import '../../../../domain/feature/answer_user.dart/entities/answer_user.dart';

class ModalBottomSheet extends StatefulWidget {
  final RxList<AnswerUser> rankAnswer;

  ModalBottomSheet({
    required this.rankAnswer,
  }) : super();

  @override
  State<ModalBottomSheet> createState() => _ModalBottomSheetState();
}

class _ModalBottomSheetState extends State<ModalBottomSheet> {
  @override
  void initState() {
    widget.rankAnswer
        .sort(((a, b) => (b.points ?? 0).compareTo(a.points ?? 0)));

    print(widget.rankAnswer);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      expand: false,
      initialChildSize: 0.9,
      minChildSize: 0.3,
      maxChildSize: 0.9,
      builder: ((ctx, scrollController) => rankBuilder()),
    );
  }

  Widget rankBuilder() {
    return Container(
      color: Get.theme.colorScheme.background,
      child: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: IconButton(
              icon: const Icon(
                Icons.close,
              ),
              onPressed: () {
                Get.back();
              },
            ),
          ),
          Image.asset(
            "assets/graphic/logo.png",
            width: Get.width * 0.5,
          ),
          const SizedBox(
            height: 20,
          ),
          Divider(
            height: 1,
            thickness: 3,
            color: Colors.white,
            indent: Get.width * 0.25,
            endIndent: Get.width * 0.25,
          ),
          const SizedBox(
            height: 15,
          ),
           Text(
            "Classement".toUpperCase(),
            style: const TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.normal),
          ),
          const SizedBox(
            height: 30,
          ),
          Column(
            children: List.generate(
                widget.rankAnswer.length,
                (index) => Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Get.theme.colorScheme.primary,
                            borderRadius: BorderRadius.circular(20)),
                        height: 80,
                        width: index == 0
                            ? Get.width * 0.85
                            : index == 1
                                ? Get.width * 0.82
                                : index == 2
                                    ? Get.width * 0.79
                                    : Get.width * 0.75,
                        child: Row(children: [
                          const SizedBox(
                            width: 5,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(17),
                            child: Image.network(
                              widget.rankAnswer[index].answer?.pathPhoto ??
                                  "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Question_mark.png/475px-Question_mark.png",
                              width: 70,
                              height: 70,
                              fit: BoxFit.cover,
                              errorBuilder: (ctx, obj, stacktrace) =>
                                  Image.network(
                                "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Question_mark.png/475px-Question_mark.png",
                                width: 70,
                                height: 70,
                                fit: BoxFit.cover,
                              ),
                              loadingBuilder: (ctx, wdgt, chnkevt) {
                                if (chnkevt?.cumulativeBytesLoaded ==
                                    chnkevt?.expectedTotalBytes) {
                                  return wdgt;
                                } else {
                                  return Container(
                                      height: 70,
                                      width: 70,
                                      child: const Center(
                                        child: CircularProgressIndicator(
                                          color: Colors.white,
                                        ),
                                      ));
                                }
                              },
                            ),
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Expanded(
                            child: Text(
                              widget.rankAnswer[index].answer?.labelAnswer ??
                                  "",
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ),
                          Text("#" + (index + 1).toString(),
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 35)),
                          const SizedBox(
                            width: 25,
                          ),
                        ]),
                      ),
                    )),
          ),
        ],
      )),
    );
  }
}
