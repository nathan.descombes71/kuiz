import 'package:get/get.dart';
import 'package:kuiz/domain/feature/answer/controller/answer_controller.dart';
import 'package:kuiz/domain/feature/questions/controller/questions_controller.dart';
import 'package:kuiz/infrastructure/repositories/answer_repository_impl.dart';
import 'package:kuiz/infrastructure/repositories/question_repository_impl.dart';

import '../../../domain/feature/quizz/controller/quizz_controller.dart';
import '../../../domain/feature/quizz/repositories/quizz_repositories.dart';
import '../../../infrastructure/api/rest_api_client.dart';
import '../../../infrastructure/repositories/quizz_repositories_impl.dart';
import 'edit_view_controller.dart';

class EditViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => EditViewController(
        Get.put<QuizzController>(
          QuizzController(
            quizzRepository: Get.put<QuizzRepository>(
              QuizzRepositoryImpl(
                client: Get.find<RestApiClient>().client,
              ),
            ),
          ),
        ),
        Get.put<QuestionController>(
          QuestionController(
            questionRepositoryImpl: Get.put<QuestionRepositoryImpl>(
              QuestionRepositoryImpl(
                client: Get.find<RestApiClient>().client,
              ),
            ),
          ),
        ),
        Get.put<AnswerController>(
          AnswerController(
            answerRepositoryImpl: Get.put<AnswerRepositoryImpl>(
              AnswerRepositoryImpl(
                client: Get.find<RestApiClient>().client,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
