import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kuiz/presentation/core/extensions/key_extension.dart';
import 'package:kuiz/presentation/core/helpers/utils.dart';
import 'package:kuiz/presentation/core/scaffold/loading_scaffold.dart';
import 'package:kuiz/presentation/views/edit/component/modal_bottom_sheet.dart';

import 'edit_view_controller.dart';

class EditView extends GetView<EditViewController> {
  EditView({Key? key}) : super(key: key);

  final GlobalKey stackKey = GlobalKey();

  double? pX;
  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) => Scaffold(
        backgroundColor: Get.theme.colorScheme.primary,
        body: Column(
          children: [
            Align(alignment: Alignment.centerLeft, child: IconButton(onPressed: (){
              Get.back();
            }, icon: Icon(Icons.chevron_left, color: Colors.white,)),),
            Expanded(
              flex: 4,
              child: Stack(
                fit: StackFit.expand,
                key: controller.stackKey,
                alignment: Alignment.center,
                children: List.generate(controller.listOfQuestions.length + 1,
                    (index) {
                  if (index != controller.listOfQuestions.length) {
                    return Obx(() {
                      if (!controller.containQuestions.value) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                padding: const EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(15)),
                                    border: Border.all(color: Colors.white)),
                                child: const Text(
                                  "Plus de questions disponible",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                ))
                          ],
                        );
                      } else {
                        return Obx(
                          () => AnimatedPositioned(
                            curve: Curves.decelerate,
                            bottom: 25,
                            left: controller
                                .listOfQuestions[index].xPosition?.value,
                            height: 400,
                            width: Get.width * 0.78,
                            duration: Duration(
                                milliseconds: controller.listOfQuestions[index]
                                        .durationTime?.value ??
                                    0),
                            child: Draggable(
                              maxSimultaneousDrags: 1,
                              onDragUpdate: ((details) {
                                pX ??= details.globalPosition.dx;
                                controller.positionDragX.value =
                                    details.globalPosition.dx;
                              }),
                              childWhenDragging: const SizedBox(),
                              onDragEnd: ((details) {
                                controller.positionDragX.value = 190;

                                final parentPos =
                                    controller.stackKey.globalPaintBounds;

                                if (details.offset.dx > 180 ||
                                    details.offset.dx < -80) {
                                  controller
                                          .selectedQuestions.xPosition?.value =
                                      details.offset.dx - parentPos!.left;
                                }

                                Future.delayed(const Duration(milliseconds: 20),
                                    () {
                                  controller.selectedQuestions.durationTime
                                      ?.value = 500;

                                  if (details.offset.dx > 180) {
                                    controller.selectedQuestions.xPosition
                                        ?.value = Get.width * 2;
                                    controller.goRight(2, swipe: true);
                                  } else if (details.offset.dx < -80) {
                                    controller.selectedQuestions.xPosition
                                        ?.value = Get.width * -2;
                                    controller.goLeft(1, swipe: true);
                                  }
                                });
                              }),
                              axis: Axis.horizontal,
                              feedback: Obx(() {
                                return Transform.rotate(
                                  angle: pi /
                                      4000 *
                                      (controller.positionDragX.value - 190),
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    color: Colors.transparent,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color:
                                              Get.theme.colorScheme.secondary,
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      height: 400,
                                      width: Get.width * 0.78,
                                      child: Center(
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          constraints: const BoxConstraints(
                                              minHeight: 80),
                                          width: Get.width * 0.65,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 20, 8, 20),
                                            child: Text(
                                              controller.listOfQuestions[index]
                                                      .labelQuestion ??
                                                  "",
                                              textAlign: TextAlign.center,
                                              style:
                                                  const TextStyle(fontSize: 20),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                color: Colors.transparent,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Get.theme.colorScheme.secondary,
                                      borderRadius: BorderRadius.circular(20)),
                                  height: 400,
                                  // width: 300,
                                  width: Get.width * 0.78,
                                  child: Center(
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      constraints:
                                          const BoxConstraints(minHeight: 80),
                                      width: Get.width * 0.65,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 20, 8, 20),
                                        child: Text(
                                          controller.listOfQuestions[index]
                                                  .labelQuestion ??
                                              "",
                                          textAlign: TextAlign.center,
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                    });
                  } else {
                    return Positioned(
                        top: 50,
                        child: Column(
                          children: [
                            Image.asset(
                              "assets/graphic/logo.png",
                              width: Get.width * 0.5,
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(top: 20, bottom: 10),
                              color: Colors.white,
                              height: 2,
                              width: Get.width * 0.5,
                              // thickness: 2,
                              // indent: Get.width * 0.2,
                              // endIndent: Get.width * 0.2,
                            ),
                            Text(
                              controller.viewQuizz?.labelQuizz ?? "",
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 25),
                            ),
                             Text(
                              controller.viewQuizz?.subtitle ?? "",
                              style: const TextStyle(
                                fontStyle: FontStyle.italic,
                                  color: Colors.white, fontSize: 18),
                            ),
                          ],
                        ));
                  }
                }),
              ),
            ),
            Expanded(
              child: Container(
                color: Get.theme.colorScheme.background,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      roundButton(Icons.dangerous, 60, false, 32,
                          onPressed: () {
                        debugPrint("J'aime pas du tout !");
                        controller.goLeft(1);
                      }),
                      roundButton(Icons.thumb_down, 50, false, 20,
                          onPressed: () {
                        debugPrint("J'aime pas !");
                        controller.goLeft(2);
                      }),
                      roundButton(Icons.format_list_bulleted, 70, true, 35,
                          onPressed: () {
                        int totalpoint = 0;

                        for (var answerUser in controller.listAnswerUser) {
                          totalpoint = totalpoint + (answerUser.points ?? 0);
                        }

                        if (totalpoint > 15) {
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (ctx) => ModalBottomSheet(
                                  rankAnswer: controller.listAnswerUser));
                          debugPrint("Voir le classement");
                        } else {
                          toast(context,
                              "Veuillez continuer a swipe pour avoir accès a un classement fiable !");
                        }
                      }),
                      roundButton(Icons.thumb_up, 50, false, 20, onPressed: () {
                        debugPrint("J'aime bien !");
                        controller.goRight(3);
                      }),
                      roundButton(Icons.star, 60, false, 32, onPressed: () {
                        debugPrint("J'aime beaucoup !");
                        controller.goRight(4);
                      }),
                    ]),
              ),
            ),
          ],
        ),
      ),
      onLoading: const LoadingScaffold()
    );
  }

  Widget roundButton(
      IconData iconToShow, double size, bool isMiddleButton, double iconSize,
      {required Function() onPressed}) {
    return Card(
      elevation: 7,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(80),
      ),
      child: Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: isMiddleButton
              ? Get.theme.colorScheme.secondary.withOpacity(0.9)
              : Get.theme.colorScheme.onSecondary.withOpacity(0.7),
        ),
        child: Center(
          child: IconButton(
            icon: Icon(
              iconToShow,
              size: iconSize,
              color: Colors.white,
            ),
            onPressed: onPressed,
          ),
        ),
      ),
    );
  }
}
