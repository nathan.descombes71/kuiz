import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/infrastructure/repositories/answer_repository_impl.dart';

import '../../../../infrastructure/dtos/quizz_dto.dart';

class AnswerController {
  final AnswerRepositoryImpl answerRepositoryImpl;

  AnswerController({
    required this.answerRepositoryImpl,
  });

  Future<String> initAnswer(
      {Map<String, dynamic>? queryParameters, String? quizzID, String? userID}) async {
    try {
      return await answerRepositoryImpl
          .initAnswer(queryParameters: queryParameters, quizzID: quizzID ?? "", userID: userID)
          .then((value) => value.fold((l) {
                return l;
              }, (r) => r));
    } catch (e) {
      print(e);
      throw (e);
    }
  }

  Future<String> addPoints(
      {Map<String, dynamic>? queryParameters, int? points, String? answerID, String? userID}) async {
    try {
      return await answerRepositoryImpl
          .addPoints(queryParameters: queryParameters, points: points, answerID: answerID ?? "", userID: userID)
          .then((value) => value.fold((l) {
                return l;
              }, (r) => r));
    } catch (e) {
      print(e);
      throw (e);
    }
  }
}
