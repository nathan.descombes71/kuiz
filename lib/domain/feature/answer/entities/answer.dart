import 'package:kuiz/infrastructure/dtos/answer_dto.dart';

class Answer {
  String? id;
  String? labelAnswer;
  String? pathPhoto;
  int? points;


  Answer({
    this.id,
    this.labelAnswer,
    this.pathPhoto,
    this.points,
  });
}

extension OnAnswer on Answer {
  Answer copyWith({
    String? id,
    String? labelAnswer,
    String? pathPhoto,
    int? points,
  }) {
    return Answer(
      id: id ?? this.id,
      labelAnswer: labelAnswer ?? this.labelAnswer,
      pathPhoto: pathPhoto ?? this.pathPhoto,
      points: points ?? this.points,
    );
  }

  AnswerDto get toDto {
    return AnswerDto(
      id: id ?? "",
      label: labelAnswer ?? "",
       pathPhoto: pathPhoto ?? "",
       points: points ?? 0,
    );
  }
}

extension OnListAnswer on List<Answer> {
  List<AnswerDto> get toDto {
    List<AnswerDto> answerDtoList = [];

    forEach((entity) => answerDtoList.add(entity.toDto));
    return answerDtoList;
  }
}

extension OnListAnswerDto on List<AnswerDto> {
  List<Answer> get toEntity {
    List<Answer> answerList = [];

    forEach((dto) => answerList.add(dto.toEntity));
    return answerList;
  }
}

extension OnAnswerDto on AnswerDto {
  Answer get toEntity {
    return Answer(
      id: id,
      labelAnswer: label,
      pathPhoto: pathPhoto,
      points: points,
    );
  }
}
