import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/infrastructure/repositories/answer_repository_impl.dart';

import '../../../../infrastructure/dtos/quizz_dto.dart';
import '../../../../infrastructure/repositories/question_repository_impl.dart';

class QuestionController {
  final QuestionRepositoryImpl questionRepositoryImpl;

  QuestionController({
    required this.questionRepositoryImpl,
  });

  Future<String> initQuestion(
      {Map<String, dynamic>? queryParameters, String? quizzID, String? userID}) async {
    try {
      return await questionRepositoryImpl
          .initQuestion(queryParameters: queryParameters, quizzID: quizzID ?? "", userID: userID)
          .then((value) => value.fold((l) {
                return l;
              }, (r) => r));
    } catch (e) {
      print(e);
      throw (e);
    }
  }

    Future<String> answerQuestion(
      {Map<String, dynamic>? queryParameters, String? questionID, String? userID}) async {
    try {
      return await questionRepositoryImpl
          .answerQuestion(queryParameters: queryParameters, questionID: questionID ?? "", userID: userID)
          .then((value) => value.fold((l) {
                return l;
              }, (r) => r));
    } catch (e) {
      print(e);
      throw (e);
    }
  }
}
