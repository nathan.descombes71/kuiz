
import 'package:get/get.dart';
import 'package:kuiz/infrastructure/dtos/question_dto.dart';

class Questions {
  String? id;
  bool? isAnswered;
  String? labelQuestion;
  RxDouble? xPosition;
  RxInt? durationTime;
  String? idAnswer;

  Questions({
    this.id,
    this.isAnswered,
    this.labelQuestion,
    this.xPosition,
    this.durationTime,
    this.idAnswer,
  });
}

extension OnQuestions on Questions {
  Questions copyWith(
      {  String? id,
  bool? isAnswered,
  String? labelQuestion,
  RxDouble? xPosition,
  RxInt? durationTime,
  String? idAnswer,
}) {
    return Questions(
        id: id ?? this.id,
        isAnswered: isAnswered ?? this.isAnswered,
        labelQuestion: labelQuestion ?? this.labelQuestion,
        xPosition: xPosition ?? this.xPosition,
        durationTime: durationTime ?? this.durationTime,
        idAnswer: idAnswer ?? this.idAnswer,
        );
  }

  QuestionDto get toDto {
    return QuestionDto(
      id: id ?? "",
      label: labelQuestion ?? "",
      isAnswered: isAnswered ?? false,
      idAnswer: idAnswer ?? "",
    );
  }
}

extension OnListQuestion on List<Questions> {
  List<QuestionDto> get toDto {
    List<QuestionDto> questionsDtoList = [];

    forEach((entity) => questionsDtoList.add(entity.toDto));
    return questionsDtoList;
  }
}

extension OnListQuestionsDto on List<QuestionDto> {
  List<Questions> get toEntity {
    List<Questions> questionList = [];

    forEach((dto) => questionList.add(dto.toEntity));
    return questionList;
  }
}

extension OnQuestionsDto on QuestionDto {
  Questions get toEntity {
    return Questions(
      id: id,
      labelQuestion: label,
      isAnswered: isAnswered,
      idAnswer: idAnswer ,
    );
  }
}
