import 'package:get/get.dart';
import 'package:kuiz/domain/feature/answer/entities/answer.dart';

class AnswerUser {
  String? idAnswer;
  int? points;
  String? idUser;
  Answer? answer;

  AnswerUser({
    this.idAnswer,
    this.points,
    this.idUser,
    this.answer,
  });
}

extension OnAnswerUser on AnswerUser {
  AnswerUser copyWith({
    String? idAnswer,
    int? points,
    String? idUser,
    Answer? answer,
  }) {
    return AnswerUser(
      idAnswer: idAnswer ?? this.idAnswer,
      points: points ?? this.points,
      idUser: idUser ?? this.idUser,
      answer: answer ?? this.answer,
    );
  }

  // QuestionsDto get toDto {
  //   return QuestionsDto(
  //     id: id ?? "",
  //     amount: amount,
  //     status: status,
  //     emailCustomer: emailCustomer,
  //     timeStamp: created,
  //   );
  // }
}

// extension OnListQuestions on List<Questions> {
//   List<QuestionsDto> get toDto {
//     List<QuestionsDto> QuestionsDtoList = [];

//     this.forEach((entity) => QuestionsDtoList.add(entity.toDto));
//     return QuestionsDtoList;
//   }
// }

// extension OnListQuestionsDto on List<QuestionsDto> {
//   List<Questions> get toEntity {
//     List<Questions> QuestionsList = [];

//     this.forEach((dto) => QuestionsList.add(dto.toEntity));
//     return QuestionsList;
//   }
// }

// extension OnQuestionsDto on QuestionsDto {
//   Questions get toEntity {
//     return Questions(
//       id: id,
//       amount: amount,
//       emailCustomer: emailCustomer,
//       status: status,
//       created: timeStamp,
//     );
//   }
// }
