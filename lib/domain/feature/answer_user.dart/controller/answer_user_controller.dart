import '../../answer/entities/answer.dart';
import '../entities/answer_user.dart';

class AnswerUserController {
  List<AnswerUser> listAnswerUser = [
    AnswerUser(
      idAnswer: "0dd236bc-d1ea-11ec-9d64-0242ac120002",
      points: 0,
      answer: Answer(
        id: "0dd236bc-d1ea-11ec-9d64-0242ac120002",
        labelAnswer: "Serpentard",
        pathPhoto:
            "https://static.wikia.nocookie.net/harrypotter/images/f/fd/Blason_de_Serpentard.png/revision/latest?cb=20150103152635&path-prefix=fr",
      ),
    ),
    AnswerUser(
      idAnswer: "4208cf40-d1ea-11ec-9d64-0242ac120002",
      points: 0,
      answer: Answer(
        id: "4208cf40-d1ea-11ec-9d64-0242ac120002",
        labelAnswer: "Poufsouffle",
        pathPhoto:
            "https://static.wikia.nocookie.net/harrypotter/images/d/d4/Blason_de_Poufsouffle.png/revision/latest?cb=20150103151525&path-prefix=fr",
      ),
    ),
    AnswerUser(
      idAnswer: "473d819a-d1ea-11ec-9d64-0242ac120002",
      points: 0,
      answer: Answer(
        id: "473d819a-d1ea-11ec-9d64-0242ac120002",
        labelAnswer: "Gryffondor",
        pathPhoto:
            "https://static.wikia.nocookie.net/harrypotter/images/8/81/Blason_de_Gryffondor.png/revision/latest/scale-to-width-down/217?cb=20130818150749&path-prefix=fr",
      ),
    ),
    AnswerUser(
      idAnswer: "4b3ee022-d1ea-11ec-9d64-0242ac120002",
      points: 0,
      answer: Answer(
        id: "4b3ee022-d1ea-11ec-9d64-0242ac120002",
        labelAnswer: "Serdaigle",
        pathPhoto:
            "https://static.wikia.nocookie.net/harrypotter/images/0/0f/Blason_de_Serdaigle.png/revision/latest?cb=20150103152520&path-prefix=fr",
      ),
    ),
  ];
}
