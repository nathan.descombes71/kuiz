import 'package:dartz/dartz.dart';
import 'package:kuiz/infrastructure/dtos/quizz_dto.dart';

abstract class QuizzRepository {
  Future<Either<String, QuizzDto>> getOneQuizz({Map<String, dynamic>? queryParameters, String quizzId});
  Future<Either<String, List<QuizzDto>>> getAllQuizz({Map<String, dynamic>? queryParameters});
}
