import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';

import '../../../../infrastructure/dtos/quizz_dto.dart';
import '../repositories/quizz_repositories.dart';

class QuizzController {
  final QuizzRepository quizzRepository;

  QuizzController({
    required this.quizzRepository,
  });

  Future<Quizz> getOneQuizz(
      {Map<String, dynamic>? queryParameters, String? quizzID}) async {
    try {
      return await quizzRepository
          .getOneQuizz(queryParameters: queryParameters, quizzId: quizzID ?? "")
          .then((value) => value.fold((l) {
                return Quizz();
              }, (r) => r.toEntity));
    } catch (e) {
      print(e);
      throw (e);
    }
  }

   Future<List<Quizz>> getAllQuizz(
      {Map<String, dynamic>? queryParameters}) async {
    try {
      return await quizzRepository
          .getAllQuizz(queryParameters: queryParameters)
          .then((value) => value.fold((l) {
                return [];
              }, (r) => r.toEntity));
    } catch (e) {
      print(e);
      throw (e);
    }
  }
}
