import 'package:get/get.dart';
import 'package:kuiz/domain/feature/questions/entities/questions.dart';
import 'package:kuiz/infrastructure/dtos/answer_dto.dart';
import '../../../../infrastructure/dtos/quizz_dto.dart';
import '../../answer/entities/answer.dart';

class Quizz {
  String? id;
  String? labelQuizz;
  String? pathPhoto;
  List<Questions>? questionsList;
  List<Answer>? answersList;
  String? subtitle;

  Quizz({
    this.id,
    this.labelQuizz,
    this.pathPhoto,
    this.answersList,
    this.questionsList,
    this.subtitle,
  });
}

extension OnQuizz on Quizz {
  Quizz copyWith({
    String? id,
    String? labelQuizz,
    String? pathPhoto,
    List<Questions>? questionsList,
    List<Answer>? answersList,
    String? subtitle,
  }) {
    return Quizz(
      id: id ?? this.id,
      labelQuizz: labelQuizz ?? this.labelQuizz,
      pathPhoto: pathPhoto ?? this.pathPhoto,
      answersList: answersList ?? this.answersList,
      questionsList: questionsList ?? this.questionsList,
      subtitle: subtitle ?? this.subtitle,
    );
  }

  QuizzDto get toDto {
    return QuizzDto(
        id: id ?? "",
        answerList: answersList?.toDto ?? [],
        label: labelQuizz ?? "",
        pathPhoto: pathPhoto ?? "",
        questionList: questionsList?.toDto ?? [],
        subtitle: subtitle ?? "");
  }
}

extension OnListQuizz on List<Quizz> {
  List<QuizzDto> get toDto {
    List<QuizzDto> QuizzDtoList = [];

    forEach((entity) => QuizzDtoList.add(entity.toDto));
    return QuizzDtoList;
  }
}

extension OnListQuizzDto on List<QuizzDto> {
  List<Quizz> get toEntity {
    List<Quizz> quizzList = [];

    forEach((dto) => quizzList.add(dto.toEntity));
    return quizzList;
  }
}

extension OnQuizzDto on QuizzDto {
  Quizz get toEntity {
    return Quizz(
      id: id,
      labelQuizz: label ?? "",
      pathPhoto: pathPhoto,
      answersList: answerList?.toEntity  ?? [],
      questionsList: questionList?.toEntity ?? [],
      subtitle: subtitle ?? "",
    );
  }
}
