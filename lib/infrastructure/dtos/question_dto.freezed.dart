// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'question_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

QuestionDto _$QuestionDtoFromJson(Map<String, dynamic> json) {
  return _QuestionDto.fromJson(json);
}

/// @nodoc
mixin _$QuestionDto {
  @JsonKey(name: 'id', includeIfNull: false)
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @JsonKey(name: 'id_answer')
  String? get idAnswer => throw _privateConstructorUsedError;
  @JsonKey(name: 'id_opposite_answer')
  String? get idOppositeAnswer => throw _privateConstructorUsedError;
  @JsonKey(name: 'isAnswered')
  bool? get isAnswered => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuestionDtoCopyWith<QuestionDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionDtoCopyWith<$Res> {
  factory $QuestionDtoCopyWith(
          QuestionDto value, $Res Function(QuestionDto) then) =
      _$QuestionDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) String id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'id_answer') String? idAnswer,
      @JsonKey(name: 'id_opposite_answer') String? idOppositeAnswer,
      @JsonKey(name: 'isAnswered') bool? isAnswered});
}

/// @nodoc
class _$QuestionDtoCopyWithImpl<$Res> implements $QuestionDtoCopyWith<$Res> {
  _$QuestionDtoCopyWithImpl(this._value, this._then);

  final QuestionDto _value;
  // ignore: unused_field
  final $Res Function(QuestionDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? idAnswer = freezed,
    Object? idOppositeAnswer = freezed,
    Object? isAnswered = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      idAnswer: idAnswer == freezed
          ? _value.idAnswer
          : idAnswer // ignore: cast_nullable_to_non_nullable
              as String?,
      idOppositeAnswer: idOppositeAnswer == freezed
          ? _value.idOppositeAnswer
          : idOppositeAnswer // ignore: cast_nullable_to_non_nullable
              as String?,
      isAnswered: isAnswered == freezed
          ? _value.isAnswered
          : isAnswered // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$$_QuestionDtoCopyWith<$Res>
    implements $QuestionDtoCopyWith<$Res> {
  factory _$$_QuestionDtoCopyWith(
          _$_QuestionDto value, $Res Function(_$_QuestionDto) then) =
      __$$_QuestionDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) String id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'id_answer') String? idAnswer,
      @JsonKey(name: 'id_opposite_answer') String? idOppositeAnswer,
      @JsonKey(name: 'isAnswered') bool? isAnswered});
}

/// @nodoc
class __$$_QuestionDtoCopyWithImpl<$Res> extends _$QuestionDtoCopyWithImpl<$Res>
    implements _$$_QuestionDtoCopyWith<$Res> {
  __$$_QuestionDtoCopyWithImpl(
      _$_QuestionDto _value, $Res Function(_$_QuestionDto) _then)
      : super(_value, (v) => _then(v as _$_QuestionDto));

  @override
  _$_QuestionDto get _value => super._value as _$_QuestionDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? idAnswer = freezed,
    Object? idOppositeAnswer = freezed,
    Object? isAnswered = freezed,
  }) {
    return _then(_$_QuestionDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      idAnswer: idAnswer == freezed
          ? _value.idAnswer
          : idAnswer // ignore: cast_nullable_to_non_nullable
              as String?,
      idOppositeAnswer: idOppositeAnswer == freezed
          ? _value.idOppositeAnswer
          : idOppositeAnswer // ignore: cast_nullable_to_non_nullable
              as String?,
      isAnswered: isAnswered == freezed
          ? _value.isAnswered
          : isAnswered // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_QuestionDto implements _QuestionDto {
  const _$_QuestionDto(
      {@JsonKey(name: 'id', includeIfNull: false) required this.id,
      @JsonKey(name: 'label') this.label,
      @JsonKey(name: 'id_answer') this.idAnswer,
      @JsonKey(name: 'id_opposite_answer') this.idOppositeAnswer,
      @JsonKey(name: 'isAnswered') this.isAnswered});

  factory _$_QuestionDto.fromJson(Map<String, dynamic> json) =>
      _$$_QuestionDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final String id;
  @override
  @JsonKey(name: 'label')
  final String? label;
  @override
  @JsonKey(name: 'id_answer')
  final String? idAnswer;
  @override
  @JsonKey(name: 'id_opposite_answer')
  final String? idOppositeAnswer;
  @override
  @JsonKey(name: 'isAnswered')
  final bool? isAnswered;

  @override
  String toString() {
    return 'QuestionDto(id: $id, label: $label, idAnswer: $idAnswer, idOppositeAnswer: $idOppositeAnswer, isAnswered: $isAnswered)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_QuestionDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.label, label) &&
            const DeepCollectionEquality().equals(other.idAnswer, idAnswer) &&
            const DeepCollectionEquality()
                .equals(other.idOppositeAnswer, idOppositeAnswer) &&
            const DeepCollectionEquality()
                .equals(other.isAnswered, isAnswered));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(label),
      const DeepCollectionEquality().hash(idAnswer),
      const DeepCollectionEquality().hash(idOppositeAnswer),
      const DeepCollectionEquality().hash(isAnswered));

  @JsonKey(ignore: true)
  @override
  _$$_QuestionDtoCopyWith<_$_QuestionDto> get copyWith =>
      __$$_QuestionDtoCopyWithImpl<_$_QuestionDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_QuestionDtoToJson(this);
  }
}

abstract class _QuestionDto implements QuestionDto {
  const factory _QuestionDto(
      {@JsonKey(name: 'id', includeIfNull: false) required final String id,
      @JsonKey(name: 'label') final String? label,
      @JsonKey(name: 'id_answer') final String? idAnswer,
      @JsonKey(name: 'id_opposite_answer') final String? idOppositeAnswer,
      @JsonKey(name: 'isAnswered') final bool? isAnswered}) = _$_QuestionDto;

  factory _QuestionDto.fromJson(Map<String, dynamic> json) =
      _$_QuestionDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  String get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'id_answer')
  String? get idAnswer => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'id_opposite_answer')
  String? get idOppositeAnswer => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'isAnswered')
  bool? get isAnswered => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_QuestionDtoCopyWith<_$_QuestionDto> get copyWith =>
      throw _privateConstructorUsedError;
}
