// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_QuestionDto _$$_QuestionDtoFromJson(Map<String, dynamic> json) =>
    _$_QuestionDto(
      id: json['id'] as String,
      label: json['label'] as String?,
      idAnswer: json['id_answer'] as String?,
      idOppositeAnswer: json['id_opposite_answer'] as String?,
      isAnswered: json['isAnswered'] as bool?,
    );

Map<String, dynamic> _$$_QuestionDtoToJson(_$_QuestionDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'id_answer': instance.idAnswer,
      'id_opposite_answer': instance.idOppositeAnswer,
      'isAnswered': instance.isAnswered,
    };
