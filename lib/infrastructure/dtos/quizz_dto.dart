import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kuiz/infrastructure/dtos/answer_dto.dart';
import 'package:kuiz/infrastructure/dtos/question_dto.dart';

part 'quizz_dto.freezed.dart';
part 'quizz_dto.g.dart';

@freezed
class QuizzDto with _$QuizzDto {
  const factory QuizzDto({
    @JsonKey(name: 'id', includeIfNull: false) required String id,
    @JsonKey(name: 'label') String? label,
    @JsonKey(name: 'path_photo') String? pathPhoto,
    @JsonKey(name: 'answer_list') List<AnswerDto>? answerList,
    @JsonKey(name: 'question_list') List<QuestionDto>? questionList,
    @JsonKey(name: 'subtitle') String? subtitle,
  }) = _QuizzDto;
  factory QuizzDto.fromJson(Map<String, dynamic> json) =>
      _$QuizzDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnQuizzJson on Map<String, dynamic> {
  QuizzDto get toQuizzDto {
    return QuizzDto.fromJson(this);
  }
}

extension OnListQuizzJson on List<Map<String, dynamic>> {
  List<QuizzDto> get toQuizzDto {
    return map<QuizzDto>((Map<String, dynamic> e) => e.toQuizzDto).toList();
  }
}
