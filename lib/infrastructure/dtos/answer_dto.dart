import 'package:freezed_annotation/freezed_annotation.dart';

part 'answer_dto.freezed.dart';
part 'answer_dto.g.dart';

@freezed
class AnswerDto with _$AnswerDto {
  const factory AnswerDto({
    @JsonKey(name: 'id', includeIfNull: false) required String id,
    @JsonKey(name: 'label') String? label,
    @JsonKey(name: 'path_photo') String? pathPhoto,
    @JsonKey(name: 'points') int? points,
  }) = _AnswerDto;
  factory AnswerDto.fromJson(Map<String, dynamic> json) =>
      _$AnswerDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnAnswerJson on Map<String, dynamic> {
  AnswerDto get toAnswerDto {
    return AnswerDto.fromJson(this);
  }
}

extension OnListAnswerJson on List<Map<String, dynamic>> {
  List<AnswerDto> get toAnswerDto {
    return map<AnswerDto>(
        (Map<String, dynamic> e) => e.toAnswerDto).toList();
  }
}
