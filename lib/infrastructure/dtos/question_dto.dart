import 'package:freezed_annotation/freezed_annotation.dart';

part 'question_dto.freezed.dart';
part 'question_dto.g.dart';

@freezed
class QuestionDto with _$QuestionDto {
  const factory QuestionDto({
    @JsonKey(name: 'id', includeIfNull: false) required String id,
    @JsonKey(name: 'label') String? label,
    @JsonKey(name: 'id_answer') String? idAnswer,
    @JsonKey(name: 'id_opposite_answer') String? idOppositeAnswer,
    @JsonKey(name: 'isAnswered') bool? isAnswered
  }) = _QuestionDto;
  factory QuestionDto.fromJson(Map<String, dynamic> json) =>
      _$QuestionDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnQuestionJson on Map<String, dynamic> {
  QuestionDto get toQuestionDto {
    return QuestionDto.fromJson(this);
  }
}

extension OnListQuestionJson on List<Map<String, dynamic>> {
  List<QuestionDto> get toQuestionDto {
    return map<QuestionDto>(
        (Map<String, dynamic> e) => e.toQuestionDto).toList();
  }
}
