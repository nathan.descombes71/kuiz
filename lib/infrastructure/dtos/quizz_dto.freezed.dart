// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'quizz_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

QuizzDto _$QuizzDtoFromJson(Map<String, dynamic> json) {
  return _QuizzDto.fromJson(json);
}

/// @nodoc
mixin _$QuizzDto {
  @JsonKey(name: 'id', includeIfNull: false)
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @JsonKey(name: 'path_photo')
  String? get pathPhoto => throw _privateConstructorUsedError;
  @JsonKey(name: 'answer_list')
  List<AnswerDto>? get answerList => throw _privateConstructorUsedError;
  @JsonKey(name: 'question_list')
  List<QuestionDto>? get questionList => throw _privateConstructorUsedError;
  @JsonKey(name: 'subtitle')
  String? get subtitle => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $QuizzDtoCopyWith<QuizzDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuizzDtoCopyWith<$Res> {
  factory $QuizzDtoCopyWith(QuizzDto value, $Res Function(QuizzDto) then) =
      _$QuizzDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) String id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'path_photo') String? pathPhoto,
      @JsonKey(name: 'answer_list') List<AnswerDto>? answerList,
      @JsonKey(name: 'question_list') List<QuestionDto>? questionList,
      @JsonKey(name: 'subtitle') String? subtitle});
}

/// @nodoc
class _$QuizzDtoCopyWithImpl<$Res> implements $QuizzDtoCopyWith<$Res> {
  _$QuizzDtoCopyWithImpl(this._value, this._then);

  final QuizzDto _value;
  // ignore: unused_field
  final $Res Function(QuizzDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? pathPhoto = freezed,
    Object? answerList = freezed,
    Object? questionList = freezed,
    Object? subtitle = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      answerList: answerList == freezed
          ? _value.answerList
          : answerList // ignore: cast_nullable_to_non_nullable
              as List<AnswerDto>?,
      questionList: questionList == freezed
          ? _value.questionList
          : questionList // ignore: cast_nullable_to_non_nullable
              as List<QuestionDto>?,
      subtitle: subtitle == freezed
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_QuizzDtoCopyWith<$Res> implements $QuizzDtoCopyWith<$Res> {
  factory _$$_QuizzDtoCopyWith(
          _$_QuizzDto value, $Res Function(_$_QuizzDto) then) =
      __$$_QuizzDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) String id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'path_photo') String? pathPhoto,
      @JsonKey(name: 'answer_list') List<AnswerDto>? answerList,
      @JsonKey(name: 'question_list') List<QuestionDto>? questionList,
      @JsonKey(name: 'subtitle') String? subtitle});
}

/// @nodoc
class __$$_QuizzDtoCopyWithImpl<$Res> extends _$QuizzDtoCopyWithImpl<$Res>
    implements _$$_QuizzDtoCopyWith<$Res> {
  __$$_QuizzDtoCopyWithImpl(
      _$_QuizzDto _value, $Res Function(_$_QuizzDto) _then)
      : super(_value, (v) => _then(v as _$_QuizzDto));

  @override
  _$_QuizzDto get _value => super._value as _$_QuizzDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? pathPhoto = freezed,
    Object? answerList = freezed,
    Object? questionList = freezed,
    Object? subtitle = freezed,
  }) {
    return _then(_$_QuizzDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      answerList: answerList == freezed
          ? _value._answerList
          : answerList // ignore: cast_nullable_to_non_nullable
              as List<AnswerDto>?,
      questionList: questionList == freezed
          ? _value._questionList
          : questionList // ignore: cast_nullable_to_non_nullable
              as List<QuestionDto>?,
      subtitle: subtitle == freezed
          ? _value.subtitle
          : subtitle // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_QuizzDto implements _QuizzDto {
  const _$_QuizzDto(
      {@JsonKey(name: 'id', includeIfNull: false) required this.id,
      @JsonKey(name: 'label') this.label,
      @JsonKey(name: 'path_photo') this.pathPhoto,
      @JsonKey(name: 'answer_list') final List<AnswerDto>? answerList,
      @JsonKey(name: 'question_list') final List<QuestionDto>? questionList,
      @JsonKey(name: 'subtitle') this.subtitle})
      : _answerList = answerList,
        _questionList = questionList;

  factory _$_QuizzDto.fromJson(Map<String, dynamic> json) =>
      _$$_QuizzDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final String id;
  @override
  @JsonKey(name: 'label')
  final String? label;
  @override
  @JsonKey(name: 'path_photo')
  final String? pathPhoto;
  final List<AnswerDto>? _answerList;
  @override
  @JsonKey(name: 'answer_list')
  List<AnswerDto>? get answerList {
    final value = _answerList;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<QuestionDto>? _questionList;
  @override
  @JsonKey(name: 'question_list')
  List<QuestionDto>? get questionList {
    final value = _questionList;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(name: 'subtitle')
  final String? subtitle;

  @override
  String toString() {
    return 'QuizzDto(id: $id, label: $label, pathPhoto: $pathPhoto, answerList: $answerList, questionList: $questionList, subtitle: $subtitle)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_QuizzDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.label, label) &&
            const DeepCollectionEquality().equals(other.pathPhoto, pathPhoto) &&
            const DeepCollectionEquality()
                .equals(other._answerList, _answerList) &&
            const DeepCollectionEquality()
                .equals(other._questionList, _questionList) &&
            const DeepCollectionEquality().equals(other.subtitle, subtitle));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(label),
      const DeepCollectionEquality().hash(pathPhoto),
      const DeepCollectionEquality().hash(_answerList),
      const DeepCollectionEquality().hash(_questionList),
      const DeepCollectionEquality().hash(subtitle));

  @JsonKey(ignore: true)
  @override
  _$$_QuizzDtoCopyWith<_$_QuizzDto> get copyWith =>
      __$$_QuizzDtoCopyWithImpl<_$_QuizzDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_QuizzDtoToJson(this);
  }
}

abstract class _QuizzDto implements QuizzDto {
  const factory _QuizzDto(
      {@JsonKey(name: 'id', includeIfNull: false) required final String id,
      @JsonKey(name: 'label') final String? label,
      @JsonKey(name: 'path_photo') final String? pathPhoto,
      @JsonKey(name: 'answer_list') final List<AnswerDto>? answerList,
      @JsonKey(name: 'question_list') final List<QuestionDto>? questionList,
      @JsonKey(name: 'subtitle') final String? subtitle}) = _$_QuizzDto;

  factory _QuizzDto.fromJson(Map<String, dynamic> json) = _$_QuizzDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  String get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'path_photo')
  String? get pathPhoto => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'answer_list')
  List<AnswerDto>? get answerList => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'question_list')
  List<QuestionDto>? get questionList => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'subtitle')
  String? get subtitle => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$$_QuizzDtoCopyWith<_$_QuizzDto> get copyWith =>
      throw _privateConstructorUsedError;
}
