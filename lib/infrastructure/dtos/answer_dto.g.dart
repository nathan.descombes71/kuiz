// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AnswerDto _$$_AnswerDtoFromJson(Map<String, dynamic> json) => _$_AnswerDto(
      id: json['id'] as String,
      label: json['label'] as String?,
      pathPhoto: json['path_photo'] as String?,
      points: json['points'] as int?,
    );

Map<String, dynamic> _$$_AnswerDtoToJson(_$_AnswerDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'path_photo': instance.pathPhoto,
      'points': instance.points,
    };
