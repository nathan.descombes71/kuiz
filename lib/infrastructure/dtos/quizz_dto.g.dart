// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quizz_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_QuizzDto _$$_QuizzDtoFromJson(Map<String, dynamic> json) => _$_QuizzDto(
      id: json['id'] as String,
      label: json['label'] as String?,
      pathPhoto: json['path_photo'] as String?,
      answerList: (json['answer_list'] as List<dynamic>?)
          ?.map((e) => AnswerDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      questionList: (json['question_list'] as List<dynamic>?)
          ?.map((e) => QuestionDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      subtitle: json['subtitle'] as String?,
    );

Map<String, dynamic> _$$_QuizzDtoToJson(_$_QuizzDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'path_photo': instance.pathPhoto,
      'answer_list': instance.answerList,
      'question_list': instance.questionList,
      'subtitle': instance.subtitle,
    };
