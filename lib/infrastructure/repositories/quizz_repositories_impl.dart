import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/infrastructure/dtos/quizz_dto.dart';
import '../../domain/feature/quizz/repositories/quizz_repositories.dart';
import '../api/rest_api_repository.dart';

class QuizzRepositoryImpl extends RestApiRepository implements QuizzRepository {
  QuizzRepositoryImpl({
    GetHttpClient? client,
  }) : super(
          controller: '/quizz',
          client: client ?? GetHttpClient(),
        );

  @override
  Future<Either<String, QuizzDto>> getOneQuizz(
      {Map<String, dynamic>? queryParameters, String? quizzId}) async {
    Response<dynamic> response = Response();

    response = await client.get("$controller/$quizzId", query: queryParameters);

    if (response.hasError) {
      return left("problème lors de la récupération du quizz");
    }

    return right(QuizzDto.fromJson(response.body));
  }

  @override
  Future<Either<String, List<QuizzDto>>> getAllQuizz(
      {Map<String, dynamic>? queryParameters, String? quizzId}) async {
    Response<dynamic> response = Response();
    List<QuizzDto> listToReturn = [];

    response = await client.get(controller, query: queryParameters);

    if (response.hasError) {
      return left("problème lors de la récupération du quizz");
    }

    listToReturn = response.body.map<QuizzDto>((e) {
        return QuizzDto.fromJson(e);
      }).toList();

    return right(listToReturn);
  }
}
