import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/infrastructure/dtos/quizz_dto.dart';
import '../../domain/feature/quizz/repositories/quizz_repositories.dart';
import '../api/rest_api_repository.dart';

class QuestionRepositoryImpl extends RestApiRepository {
  QuestionRepositoryImpl({
    GetHttpClient? client,
  }) : super(
          controller: '/question',
          client: client ?? GetHttpClient(),
        );

  Future<Either<String, String>> initQuestion(
      {Map<String, dynamic>? queryParameters,
      String? quizzID,
      String? userID}) async {
    Response<dynamic> response = Response();

    response = await client.post("$controller/$userID/$quizzID",
        query: queryParameters);

    if (response.hasError) {
      return left("error-init");
    }

    return right("success");
  }

  Future<Either<String, String>> answerQuestion(
      {Map<String, dynamic>? queryParameters,
      String? questionID,
      String? userID}) async {
    Response<dynamic> response = Response();

    response = await client.post("/answerQuestion/$userID/$questionID",
        query: queryParameters);

    if (response.hasError) {
      return left("error-init");
    }

    return right("success");
  }
}
