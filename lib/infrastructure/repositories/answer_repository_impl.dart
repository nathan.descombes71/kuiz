import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kuiz/domain/feature/quizz/entities/quizz.dart';
import 'package:kuiz/infrastructure/dtos/quizz_dto.dart';
import '../../domain/feature/quizz/repositories/quizz_repositories.dart';
import '../api/rest_api_repository.dart';

class AnswerRepositoryImpl extends RestApiRepository {
  AnswerRepositoryImpl({
    GetHttpClient? client,
  }) : super(
          controller: '/answer',
          client: client ?? GetHttpClient(),
        );

  Future<Either<String, String>> initAnswer(
      {Map<String, dynamic>? queryParameters,
      String? quizzID,
      String? userID}) async {
    Response<dynamic> response = Response();

    response = await client.post("$controller/$userID/$quizzID",
        query: queryParameters);

    if (response.hasError) {
      return left("error-init");
    }

    return right("success");
  }

  Future<Either<String, String>> addPoints(
      {Map<String, dynamic>? queryParameters,
      String? answerID,
      int? points,
      String? userID}) async {
    Response<dynamic> response = Response();

    response = await client.post("/points/$userID/$answerID",
        body: '''{
                      "points": $points
                  }''',
        query: queryParameters);

    if (response.hasError) {
      return left("error-init");
    }

    return right("success");
  }
}
